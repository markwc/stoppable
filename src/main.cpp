/// @file main.cpp

#include <Stoppable.h>

#include <array>
#include <chrono>
#include <iostream>
#include <random>
#include <thread>

class My_task : public Katas::Stoppable
{
public:
  void run()
  {
    std::cout << "Task started" << std::endl;
    while (stop_requested() == false)
    {
      std::cout << "Task working" << std::endl;
      std::this_thread::sleep_for(std::chrono::seconds(1U));
    }
    std::cout << "Task stopped" << std::endl;
  }
};

int main()
{
  std::cout << "Main run" << std::endl;
  My_task task;

  std::thread th([&]() { task.run(); });

  std::this_thread::sleep_for(std::chrono::seconds(10U));

  std::cout << "Asking Task to stop" << std::endl;
  task.request_stop();

  th.join();
  return 0;
}