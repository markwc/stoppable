/// @file Stoppable.cpp

#include <Stoppable.h>

#include <chrono>
#include <future>
#include <thread>

namespace Katas
{
struct Stoppable::Impl
{
  std::promise<void> m_exit_signal;
  std::future<void> m_future_exit_signal;

  Impl() : m_future_exit_signal(m_exit_signal.get_future())
  {
  }
};

//-----------------------------------------------------------------------------

Stoppable::Stoppable() : m_p_impl(std::make_unique<Impl>())
{
}

Stoppable::~Stoppable() = default;

bool Stoppable::stop_requested()
{
  if (m_p_impl->m_future_exit_signal.wait_for(std::chrono::milliseconds(0)) == std::future_status::timeout)
  {
    return false;
  }
  return true;
}

void Stoppable::request_stop()
{
  m_p_impl->m_exit_signal.set_value();
}
} // namespace Katas
