/// @file Stoppable.h

#ifndef SRC_STOPPABLE_H
#define SRC_STOPPABLE_H

#include <memory>

namespace Katas
{

class Stoppable
{
public:
  Stoppable();
  ~Stoppable();
  bool stop_requested();
  void request_stop();
  Stoppable(Stoppable &&) = delete;
  Stoppable(const Stoppable &) = delete;
  Stoppable &operator=(Stoppable &&) = delete;
  Stoppable &operator=(const Stoppable &) = delete;

private:
  struct Impl;
  std::unique_ptr<Impl> m_p_impl;
};
} // namespace Katas

#endif // #define SRC_STOPPABLE_H
