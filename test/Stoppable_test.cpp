#include <Stoppable.h>
#include <gtest/gtest.h>

#include <array>
#include <chrono>
#include <iostream>
#include <random>
#include <thread>

namespace
{

class Stoppable_test : public ::testing::Test
{
public:
  Stoppable_test() = default;

  ~Stoppable_test() override = default;

  Stoppable_test(Stoppable_test &&) = delete;
  Stoppable_test(const Stoppable_test &) = delete;
  Stoppable_test &operator=(Stoppable_test &&) = delete;
  Stoppable_test &operator=(const Stoppable_test &) = delete;

protected:
  void SetUp() override
  {
  }

  void TearDown() override
  {
  }
};

TEST_F(Stoppable_test, instantiate)
{
  Katas::Stoppable UUT;
  ASSERT_FALSE(UUT.stop_requested());
  UUT.request_stop();
  ASSERT_TRUE(UUT.stop_requested());
}

TEST_F(Stoppable_test, create)
{
  auto p_UUT = std::make_unique<Katas::Stoppable>();
  ASSERT_NE(nullptr, p_UUT);
  ASSERT_FALSE(p_UUT->stop_requested());
  p_UUT->request_stop();
  ASSERT_TRUE(p_UUT->stop_requested());
}

TEST_F(Stoppable_test, thread)
{
  class My_sub_task : public Katas::Stoppable
  {
  public:
    void run()
    {
      std::cout << "sub_task started" << std::endl;
      while (!stop_requested())
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(1U));
      }
      std::cout << "sub_task stopped" << std::endl;
    }
  };

  class My_task : public Katas::Stoppable
  {
  public:
    std::array<My_sub_task, 2> m_tasks;
    std::array<std::thread *, 2> m_p_threads{};

    void run()
    {
      std::cout << "task started" << std::endl;
      for (size_t i = 0; i < 2; i++)
      {
        m_p_threads[i] = new std::thread([&, i]() { m_tasks[i].run(); });
      }

      while (!stop_requested())
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(10U));
      }
      for (size_t i = 0; i < 2; i++)
      {
        m_tasks[i].request_stop();
        m_p_threads[i]->join();
      }
      std::cout << "task stopped" << std::endl;
    }
  };

  My_task task;

  std::thread th([&]() { task.run(); });

  std::this_thread::sleep_for(std::chrono::milliseconds(100U));
  task.request_stop();
  th.join();
}
} // namespace